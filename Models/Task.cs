﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WindowsTasksModules.Models
{
    [XmlRoot(Namespace = "http://schemas.microsoft.com/windows/2004/02/mit/task", IsNullable = false)]
    public class Task
    {
        public string Id => Guid.NewGuid().ToString();
        public DateTime CreateStamp { get; set; }
        public string HostName { get; set; }
        public string Name { get; set; }
        [XmlElement()]
        public RegistrationInfo RegistrationInfo { get; set; }

        [XmlElement()]
        public List<Setting> Settings { get; set; }

        [XmlElement()]
        public List<Trigger> Triggers { get; set; }

        [XmlElement()]
        public Actions Actions { get; set; }
    }

    public class RegistrationInfo
    {
        [XmlElement()]
        public DateTime Date { get; set; }
        [XmlElement()]
        public string Author { get; set; }
        [XmlElement()]
        public string Description { get; set; }
        [XmlElement()]
        public string Uri { get; set; }
    }

    public class Setting
    {
        [XmlElement()]
        public bool DisallowStartIfOnBatteries { get; set; }
        [XmlElement()]
        public bool StopIfGoingOnBatteries { get; set; }
        [XmlElement()]
        public bool Enabled { get; set; }
        [XmlElement()]
        public string ExecutionTimeLimit { get; set; }
        [XmlElement()]
        public bool Hidden { get; set; }
    }

    public class Trigger
    {
        [XmlElement()]
        public CalendarTrigger CalendarTrigger { get; set; }    
    }

    public class CalendarTrigger
    {
        [XmlElement()]
        public DateTime StartBoundary { get; set; }

        [XmlElement()]
        public ScheduleByWeek ScheduleByWeek { get; set; }

    }

    public class ScheduleByWeek
    {
        [XmlElement()]
        public int WeeksInterval { get; set; }
        [XmlElement()]
        public List<DaysOfWeek> DaysOfWeek { get; set; }
    }

    public class DaysOfWeek
    {
        [XmlElement()]
        public string Monday { get; set; }
        [XmlElement()]
        public string Tuesday { get; set; }
        [XmlElement()]
        public string Wednesday { get; set; }
        [XmlElement()]
        public string Thursday { get; set; }
        [XmlElement()]
        public string Friday { get; set; }
        [XmlElement()]
        public string Saturday { get; set; }
        [XmlElement()]
        public string Sunday { get; set; }
    }

    public class Actions
    {
        [XmlElement()]
        public List<Exec> Exec { get; set; }
    }
    public class Exec
    {
        [XmlElement()]
        public string Command { get; set; }

        [XmlElement()]
        public string Arguments { get; set; }

        [XmlElement()]
        public string WorkingDirectory { get; set; }
    }


}
