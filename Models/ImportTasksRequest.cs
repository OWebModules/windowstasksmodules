﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsTasksModules.Models
{
    public class ImportTasksRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public ImportTasksRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
