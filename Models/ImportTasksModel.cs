﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsTasksModules.Models
{
    public class ImportTasksModel
    {
        public clsOntologyItem Config { get; set; }
        public clsOntologyItem Path { get; set; }
        public clsOntologyItem Server { get; set; }
    }
}
