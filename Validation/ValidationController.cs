﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsTasksModules.Models;

namespace WindowsTasksModules.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateImportTasksRequest(ImportTasksRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The config-id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The config-id is no GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateImportTasksModel(ImportTasksModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ImportTasksModel.Config))
            {
                if (model.Config == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Config is not present";
                }

                if (model.Config.GUID_Parent != Config.LocalData.Class_Import_Windows_Tasks.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Config-Object is not of expected class!";
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ImportTasksModel.Path))
            {
                if (model.Path == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Path is not present!";
                }

                if (!System.IO.Directory.Exists(model.Path.Name))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Path is not existing!";
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ImportTasksModel.Server))
            {
                if (model.Server == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Server is not present!";
                }
            }

            return result;
        }
    }
}
