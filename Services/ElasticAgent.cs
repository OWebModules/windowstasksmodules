﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsTasksModules.Validation;

namespace WindowsTasksModules.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {
        public async Task<ResultItem<Models.ImportTasksModel>> GetImportTasksModel(Models.ImportTasksRequest request)
        {
            var taskResult = await System.Threading.Tasks.Task.Run<ResultItem<Models.ImportTasksModel>>(() =>
           {
               var result = new ResultItem<Models.ImportTasksModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new Models.ImportTasksModel()
               };

               result.ResultState = ValidationController.ValidateImportTasksRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig
                   }
               };

               var dbReaderConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while gettint the config!";
                   return result;
               }

               result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateImportTasksModel(result.Result, globals, nameof(Models.ImportTasksModel.Config));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchPath = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Import_Windows_Tasks_uses_Path.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Import_Windows_Tasks_uses_Path.ID_Class_Right
                   }
               };

               var dbReaderPath = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPath.GetDataObjectRel(searchPath);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Path!";
                   return result;
               }

               result.Result.Path = dbReaderPath.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               result.ResultState = ValidationController.ValidateImportTasksModel(result.Result, globals, nameof(Models.ImportTasksModel.Path));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchServer = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Import_Windows_Tasks_belonging_Server.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Import_Windows_Tasks_belonging_Server.ID_Class_Right
                   }
               };

               var dbReaderServer = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderServer.GetDataObjectRel(searchServer);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Server!";
                   return result;
               }
               result.Result.Server = dbReaderServer.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               result.ResultState = ValidationController.ValidateImportTasksModel(result.Result, globals, nameof(Models.ImportTasksModel.Server));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public ElasticAgent(Globals globals): base(globals)
        {

        }
        
    }
}
