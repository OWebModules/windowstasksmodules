﻿using ElasticSearchNestConnector;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WindowsTasksModules.Validation;

namespace WindowsTasksModules
{
    public class WindowsTasksConnector : AppController
    {
        public async Task<ResultItem<List<Models.Task>>> ImportTasks(Models.ImportTasksRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<Models.Task>>>(async() =>
           {
               var result = new ResultItem<List<Models.Task>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<Models.Task>()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateImportTasksRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Validated request.");

               var serviceAgent = new Services.ElasticAgent(Globals);

               request.MessageOutput?.OutputInfo("Get model...");

               var serviceResult = await serviceAgent.GetImportTasksModel(request);

               result.ResultState = serviceResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Have model.");

               request.MessageOutput?.OutputInfo("Get Elasticsearch Index and Type...");
               var elasticSearchConfigController = new ElasticSearchConfigModule.ElasticSearchConfigController(Globals);
               var configResult = await elasticSearchConfigController.GetConfig(serviceResult.Result.Config, Config.LocalData.RelationType_uses, Config.LocalData.RelationType_uses, Globals.Direction_LeftRight);
               result.ResultState = configResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Have Elasticsearch Index and Type.");

               var serializer = new XmlSerializer(typeof(Models.Task));

               var path = serviceResult.Result.Path.Name;
               var server = serviceResult.Result.Server.Name;
               var createStamp = DateTime.Now;
               try
               {
                   var files = System.IO.Directory.GetFiles(path, "*.xml");

                   request.MessageOutput?.OutputInfo($"Scan {files.Count()} files...");
                   foreach (var filePath in files)
                   {
                       using (var fileReader = new FileStream(filePath, FileMode.Open))
                       {
                           var taskItem = (Models.Task)serializer.Deserialize(fileReader);
                           taskItem.Name = Path.GetFileNameWithoutExtension(filePath);
                           taskItem.HostName = server;
                           taskItem.CreateStamp = createStamp;

                           result.Result.Add(taskItem);
                       }
                   }
                   request.MessageOutput?.OutputInfo($"Scanned files. Found {result.Result.Count} tasks.");

               }
               catch (Exception ex)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = ex.Message;
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var dbReader = new clsUserAppDBSelector(configResult.Result.NameServer, configResult.Result.Port, configResult.Result.NameIndex, Globals.SearchRange, Globals.Session);
               var dbWriter = new clsUserAppDBUpdater(dbReader);

               if (result.Result.Any())
               {
                   request.MessageOutput?.OutputInfo($"Save Session-Item...");
                   result.ResultState = dbWriter.SaveDoc<Models.Task>(result.Result, nameof(Models.Task.Id), configResult.Result.NameType);
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var sessionItem = new clsOntologyItem
                   {
                       GUID = Globals.NewGUID,
                       Name = createStamp.ToString("yyyy-MM-ddTHH:mm:ss"),
                       GUID_Parent = Config.LocalData.Class_Import_Windows_Tasks__Session_.GUID,
                       Type = Globals.Type_Object
                   };

                   var relationConfig = new clsRelationConfig(Globals);

                   var dateTimeAttribute = relationConfig.Rel_ObjectAttribute(sessionItem, Config.LocalData.AttributeType_DateTimestamp, createStamp);

                   var createSessionItemResult = await serviceAgent.SaveObjects(new List<clsOntologyItem> { sessionItem });
                   result.ResultState = createSessionItemResult;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the Session-Item!";
                       return result;
                   }

                   var createAttributeResult = await serviceAgent.SaveAttributes(new List<clsObjectAtt>
                   {
                       dateTimeAttribute
                   });

                   result.ResultState = createAttributeResult;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the Session-Item!";
                       return result;
                   }

                   var relationItem = relationConfig.Rel_ObjectRelation(sessionItem, serviceResult.Result.Config, Config.LocalData.RelationType_belongs_to);

                   var createRelResult = await serviceAgent.SaveRelations(new List<clsObjectRel>
                   {
                       relationItem
                   });

                   result.ResultState = createRelResult;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the relation between the Session-Item to configuration!";
                       return result;
                   }

                   request.MessageOutput?.OutputInfo($"Saved Session-Item.");
               }
               
               return result;
           });

            return taskResult;
        }

        public WindowsTasksConnector(Globals globals) : base(globals)
        {
        }
    }
}
